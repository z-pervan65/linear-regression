//
// Created by zvonimir on 21.07.18..
//
#include <iostream>
#include <vector>
#include <math.h>
#include "../include/LinearRegression.h"

LinearRegression::LinearRegression() : averageX{0},
                                       averageY{0},
                                       b1{0},
                                       sumNumerator{0},
                                       sumDenominator{0} {};

LinearRegression::~LinearRegression() {}

/*
 * Calculate the average value of the X elements
 * Formula:
 *
 */
double LinearRegression::calculateAverageX(std::vector<double> &valueX)
{
  std::cout << "Calculating average X" << std::endl;
  for (int i = 0; i < valueX.size(); i++) {
    averageX += valueX.at(i);
  }

  averageX = (averageX) / valueX.size();
  std::cout << "Done!" << std::endl;

  return averageX;
}

/*
 * Calculate the average value of the Y elements
 */
double LinearRegression::calculateAverageY(std::vector<double> &valueY)
{
  std::cout << "Calculating average Y" << std::endl;

  for (int i = 0; i < valueY.size(); i++) {
    averageY += valueY.at(i);
  }

  averageY = (averageY) / valueY.size();
  std::cout << "Done!" << std::endl;

  return averageY;
}

/*
 * Calculate coefficent B1
 */
double LinearRegression::calculateCoefB1(std::vector<double> &valueX,
                                         std::vector<double> &valueY,
                                         double &averageX,
                                         double &averageY) {
  std::cout << "Calculating coefficient B1..." << std::endl;

  for (int i = 0; i < valueX.size(); i++) {
    resultX.push_back(valueX.at(i) - averageX);
    resultY.push_back(valueY.at(i) - averageY);
    resultPowX.push_back(std::pow(resultX.at(i), 2));
    sumNumerator += (resultX.at(i) * resultY.at(i));
    sumDenominator += resultPowX.at(i);
  }

  b1 = sumNumerator / sumDenominator;
  std::cout << "Done!" << std::endl;

  return b1;
}

double LinearRegression::calculateCoefB0(double &averageX, double &averageY, double &B1)
{
    std::cout << "Calculating B0" << std::endl;
    double B0 = 0;
    B0 = averageY - (B1 * averageX);
    std::cout << "Done!" << std::endl;

    return B0;
}

double LinearRegression::roundTwoDecimals(double* number)
{
    double roundedNumber = 0;
    roundedNumber = floor(*number * 100) / 100;
    return roundedNumber;
}