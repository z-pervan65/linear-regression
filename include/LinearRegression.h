//
// Created by zvonimir on 21.07.18..
//

#ifndef LINEARREGRESSION_LINEARREGRESSION_H
#define LINEARREGRESSION_LINEARREGRESSION_H

#include <vector>

class LinearRegression {
    public:
        LinearRegression();
        ~LinearRegression();
        double calculateAverageX(std::vector<double>& valueX);
        double calculateAverageY(std::vector<double>& valueY);
        double calculateCoefB1(std::vector<double>& valueX, std::vector<double>& valueY, double& averageX, double& averageY);
        double calculateCoefB0(double& averageY, double& averageX, double& B1);
        double roundTwoDecimals(double* number);

    private:
        double averageX;
        double averageY;
        double b1;
        double sumNumerator;
        double sumDenominator;
        std::vector<double> resultX;
        std::vector<double> resultY;
        std::vector<double> resultPowX;
};


#endif //LINEARREGRESSION_LINEARREGRESSION_H
